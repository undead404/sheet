#!/usr/bin/env python3


class Sheet:
	# class variable stores
	assortment = {'sandwich' : {'price': 10.0, 'primecost': 4.0},
		'hotdog': {'price': 20.0, 'primecost': 8.0},
		'oatmeal': {'price': 8.0, 'primecost': 3.0},
		'egg': {'price': 1.0, 'primecost': 0.4}}
	purchases = None

	def __init__(self):
		self.purchases = []

	def add_purchase(self, purchase):
		"""
		Adds a purchase to the sheet.
		"""
		# check if there is such goods in the assortment
		Sheet.assortment[purchase[0]]
		self.purchases.append(purchase)

	def add_purchases(self, purchases):
		"""
		Adds goods to the sheet
		"""
		for purchase in purchases:
			self.add_purchase(purchase)

	def calc_profit(self):
		"""
		Calculates total profit in the sheet.
		"""
		profit = 0.0
		for purchase in self.purchases:
			purchase_name, purchase_quantity = purchase
			profit += purchase_quantity * (Sheet.assortment[purchase_name][
				'price'] - Sheet.assortment[purchase_name]['primecost'])
		return profit

	
if __name__ == '__main__':
	sheet = Sheet()
	wishes = [("sandwich", 1),
                ("hotdog", 2),
                ("oatmeal", 3),
                ("egg", 10)]
	# print('Dear paymaster, enter here the name of goods and how much of it should be bought:')
	sheet.add_purchases(purchases=wishes)
	print("Total profit: {profit}".format(profit=sheet.calc_profit()))

